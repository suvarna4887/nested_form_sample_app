class CricketTeam < ActiveRecord::Base
	has_many :players
	accepts_nested_attributes_for :players, :reject_if => lambda { |a| a[:content].blank? }, :allow_destroy => true

	def player_name
		players.collect{|p| p.name}.join(", ")
	end

end
