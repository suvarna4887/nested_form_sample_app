json.array!(@cricket_teams) do |cricket_team|
  json.extract! cricket_team, :id, :title, :content
  json.url cricket_team_url(cricket_team, format: :json)
end
