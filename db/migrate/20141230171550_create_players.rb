class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :name
      t.text :content
      t.references :cricket_team, index: true

      t.timestamps
    end
  end
end
