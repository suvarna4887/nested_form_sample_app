class CreateCricketTeams < ActiveRecord::Migration
  def change
    create_table :cricket_teams do |t|
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
