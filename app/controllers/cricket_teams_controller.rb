class CricketTeamsController < ApplicationController
  before_action :set_cricket_team, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @cricket_teams = CricketTeam.all
    respond_with(@cricket_teams)
  end

  def show
    respond_with(@cricket_team)
  end

  def new
    @cricket_team = CricketTeam.new
    respond_with(@cricket_team)
  end

  def edit
  end

  def create
    @cricket_team = CricketTeam.new(cricket_team_params)
    @cricket_team.save
    redirect_to cricket_teams_path
  end

  def update
    @cricket_team.update(cricket_team_params)
    respond_with(@cricket_team)
  end

  def destroy
    @cricket_team.destroy
    respond_with(@cricket_team)
  end

  private
    def set_cricket_team
      @cricket_team = CricketTeam.find(params[:id])
    end

    def cricket_team_params
      params.require(:cricket_team).permit(:title, :content, players_attributes: [:id, :name, :content, :_destroy])
    end
end
