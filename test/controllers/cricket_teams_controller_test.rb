require 'test_helper'

class CricketTeamsControllerTest < ActionController::TestCase
  setup do
    @cricket_team = cricket_teams(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cricket_teams)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cricket_team" do
    assert_difference('CricketTeam.count') do
      post :create, cricket_team: { content: @cricket_team.content, title: @cricket_team.title }
    end

    assert_redirected_to cricket_team_path(assigns(:cricket_team))
  end

  test "should show cricket_team" do
    get :show, id: @cricket_team
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cricket_team
    assert_response :success
  end

  test "should update cricket_team" do
    patch :update, id: @cricket_team, cricket_team: { content: @cricket_team.content, title: @cricket_team.title }
    assert_redirected_to cricket_team_path(assigns(:cricket_team))
  end

  test "should destroy cricket_team" do
    assert_difference('CricketTeam.count', -1) do
      delete :destroy, id: @cricket_team
    end

    assert_redirected_to cricket_teams_path
  end
end
